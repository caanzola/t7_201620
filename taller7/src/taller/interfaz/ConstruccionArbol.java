package taller.interfaz;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Properties;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import netscape.javascript.JSException;
import netscape.javascript.JSObject;
import jdk.nashorn.internal.ir.debug.JSONWriter;
import Estructuras.ArbolBinario;
import Estructuras.NodoArbol;

public class ConstruccionArbol implements IReconstructorArbol
{

	private ArbolBinario<String , String> arbolBinario;
	String lineaPreorden;
	String  lineaInorden;
	String [] preorden;
	String []  inorden;

	public ConstruccionArbol() 
	{
		arbolBinario = new ArbolBinario<>();
	}

	@Override
	public void cargarArchivo(String nombre) throws IOException 
	{
		Properties datos = leerArchivo(nombre);

		lineaPreorden = datos.getProperty("preorden");
		preorden = lineaPreorden.split(",");
		lineaInorden = datos.getProperty("inorden");
		inorden = lineaInorden.split(",");


	}


	@Override
	public void reconstruir() 
	{
		int h = 0;
		boolean sigaQueSiHay;
		int indicador = 9;

		while(h < preorden.length)
		{
			sigaQueSiHay = true;
			int contadorIzquierdo = 0;
			int contadorDerecho = 0;
			int i = 0;
			while(i < inorden.length)
			{
				if(!preorden[h].equals(inorden[i]) && sigaQueSiHay)
				{
					contadorIzquierdo++;
				}
				else if(preorden[h].equals(inorden[i]))
				{
					for (int j = i+1; j < indicador; j++) 
					{
						contadorDerecho++;
					}
					sigaQueSiHay = false;
					indicador = i;
				}

				i++;
			}
			String aInsertar = construirArbol(inorden, preorden, h, contadorIzquierdo,contadorDerecho);
			arbolBinario.put(aInsertar, aInsertar);
			h++;
		}

		arbolBinario.imprimirArbol(arbolBinario.getRoot());
		ArrayList<String> objetos = arbolBinario.getObjects();
		ArrayList<String> nodos = arbolBinario.getNodes();
		String nombreJSon = generarJson(objetos, nodos);

		System.out.println("Ha plantado el árbol con éxito!\nPara verlo, dirijase a /data/arbolPlantado.json");
		System.out.println("Nota: ejecute Refresh (Clic derecho - Refresh) sobre la carpeta /data/ para actualizar y visualizar el archivo JSON");
		System.out.println("Presione 1 para salir");
	}

	public String generarJson(ArrayList<String> pObjetos, ArrayList<String> pNodos) 
	{
		String nombreArchivo = null;
		try {
			JSONObject obj = new JSONObject();
			int k = 0;
			while(k < pObjetos.size())
			{
				obj.put("Nodo " + (k+1), pNodos.get(k) + " " + pObjetos.get(k));
				k++;
			}

			File file = new File("./data/arbol.json");
			nombreArchivo = "./data/arbol.json";
			file.createNewFile();  
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(obj.toJSONString());  
			fileWriter.flush();  
			fileWriter.close();  

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return nombreArchivo;
	}

	public Properties leerArchivo(String nombre)  
	{	
		File archivo = new File(nombre);
		Properties datos = new Properties( );
		FileInputStream entra = null;
		try
		{
			entra = new FileInputStream( archivo );
		}
		catch(Exception e)
		{
			System.out.println( "El archivo no existe");
		}
		try
		{
			datos.load( entra );
			entra.close();
		}
		catch(Exception e)
		{
			System.out.println("El archivo no tiene el formato esperado.");
		}

		return datos;
	}

	public String construirArbol(String[] inorden, String[] preorden, int posRaizActual, int tamanioArregloIzq, int tamanioArregloDer) 
	{
		String[] tempIzq = new String[tamanioArregloIzq];
		String[] tempDer = new String[tamanioArregloDer];
		NodoArbol<String, String> nodoIzquierdo;
		NodoArbol<String, String> nodoDerecho;
		String dev = null;
		int contadorIzq = 0;
		int contadorDer = 0;

		if(tamanioArregloIzq != 0 && tamanioArregloDer != 0)
		{
			int i = 0;
			boolean sigaQueSiHay = true;
			while(i < inorden.length)
			{
				if(!preorden[posRaizActual].equals(inorden[i]) && sigaQueSiHay)
				{
					tempIzq[i] = inorden[i];
					contadorIzq++;
				}
				else if(preorden[posRaizActual].equals(inorden[i]))
				{
					dev = inorden[i];
					for (int j = 0; j < tempDer.length && (i+j+1) < inorden.length; j++) 
					{
						contadorDer++;
						tempDer[j] = inorden[i+j+1];
					}
					sigaQueSiHay = false;
				}
				i++;
			}
			
		}
		else 
		{
			int i = 0;
			while(i < inorden.length)
			{
				if(preorden[posRaizActual].equals(inorden[i]))
				{
					dev = inorden[i];
				}
				i++;
			}
		}
		return dev;
	}

}
