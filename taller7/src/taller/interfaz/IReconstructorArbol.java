package taller.interfaz;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.json.simple.parser.ParseException;

public interface IReconstructorArbol {

	/**
	 * Este es el método que carga el archivo
	 * @param nombre es el nombre del archivo a cargar (con .properties incluido)
	 * @throws IOException si no se puede cargar el archivo
	 */
	public void cargarArchivo(String nombre) throws IOException;
	
	/**
	 * Este es el método que reconstruye el árbol.El inorden y postorden son
	 * atributos del mundo.
	 */
	public void reconstruir();

}
