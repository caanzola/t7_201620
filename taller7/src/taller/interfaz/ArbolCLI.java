package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Properties;
import java.util.Scanner;

import Estructuras.ArbolBinario;
import Estructuras.NodoArbol;

public class ArbolCLI {

	private Scanner in;
	private ConstruccionArbol constructor;

	public ArbolCLI()
	{
		in = new Scanner(System.in);
	}

	public void mainMenu()
	{
		boolean finish = false;
		while(!finish)
		{	
			Screen.clear();
			System.out.println("------------------------------------------");
			System.out.println("-                                        -");
			System.out.println("-           Siembra de árboles           -");
			System.out.println("-                                        -");
			System.out.println("------------------------------------------");
			System.out.println("EL sistema para la plantación de árboles binarios\n");

			System.out.println("Menú principal:");
			System.out.println("-----------------");
			System.out.println("1. Cargar archivo con semillas");
			System.out.println("2. Salir");
			System.out.print("\nSeleccione una opción: ");
			int opt1 = in.nextInt();
			switch(opt1)
			{
			case 1:
				recibirArchivo();
				finish = true;
				break;
			case 2:
				finish = true;
				break;
			default:
				break;
			}
		}
	}

	public void recibirArchivo()
	{
		boolean finish = false;
		while(!finish)
		{
			//Screen.clear();
			System.out.println("Recuerde que el archivo a cargar");
			System.out.println("debe ser un archivo properties");
			System.out.println("que tenga la propiedad in-orden,");
			System.out.println("la propiedad pre-orden (donde los ");
			System.out.println("elementos estén separados por comas) y");
			System.out.println("que esté guardado en la carpeta data.");
			System.out.println("");
			System.out.println("Introduzca el nombre del archivo:");
			System.out.println("----------------------------------------------------");

			// TODO Leer el archivo .properties 
			
			String nombre = "./data/"+in.next()+".properties";
			constructor = new ConstruccionArbol();

			try 
			{

				constructor. cargarArchivo(nombre);
				
				// TODO Reconstruir árbol
				constructor.reconstruir();  

				in.next();
				finish=true;

			} catch (Exception e) {
				System.out.println("Hubo un problema cargando el archivo:");
				System.out.println(e.getMessage());
				e.printStackTrace();

			}
		}
	}

} 