package Estructuras;

public class NodoArbol<K, V> 
{
	private K key;
	private V value;
	private NodoArbol<K,V> left;
	private NodoArbol<K,V> rigth;
	
	public NodoArbol(K pkey, V pvalue, NodoArbol<K, V> pLeft, NodoArbol<K, V> pRight) 
	{
	   key = pkey;
	   value = pvalue;
	   left = pLeft;
	   rigth = pRight;
	}
	
	public K getKey()
	{
		return key;
	}
	
	public V getValue()
	{
		return value;
	}
	
	public void setValue(V newValue)
	{
		value = newValue;
	}
	
	public NodoArbol<K, V> getLeft()
	{
		return left;
	}
	
	public NodoArbol<K, V> getRight()
	{
		return rigth;
	}
	
	public void setLeft(NodoArbol<K, V> newLeft)
	{
		left = newLeft;
	}
	
	public void setRight(NodoArbol<K, V> newRight)
	{
		rigth = newRight;
	}
	

}
