package Estructuras;

import java.util.ArrayList;

public class ArbolBinario <K extends Comparable<K>, V>
{
	private NodoArbol<K,V> root;
	private ArrayList<V> objetos;
	private ArrayList<V> nodos;
	private String cadena;

	public ArbolBinario() 
	{
		root = null;
		objetos = new ArrayList<>();
		nodos = new ArrayList<>();
	}
	
	public void put(K key, V value)
	{
		root = put(root,key,value);
	}
	
	public NodoArbol<K, V> getRoot()
	{
		return root;
	}

	public NodoArbol<K, V> put(NodoArbol<K, V> actual, K pkey, V pvalue) 
	{
		if(actual == null)
		{
			return new NodoArbol<K,V>(pkey, pvalue, null, null);
		}
		else
		{
			if(pkey.compareTo(actual.getKey())<0)
			{
				actual.setLeft(put(actual.getLeft(), pkey, pvalue));
			}
			else if(pkey.compareTo(actual.getKey())>0)
			{
				actual.setRight(put(actual.getRight(), pkey, pvalue));
			}
			else
			{
				actual.setValue(pvalue);
			}
			
			return actual;
		}
	}

	public void imprimirArbol(NodoArbol<K, V> raizActual) 
	{	
		if(raizActual != null)
		{
			System.out.println();
			System.out.println("Imprimiendo");
			System.out.println("Raiz = "+raizActual.getValue());
			objetos.add((V) ("Raiz = " + raizActual.getValue()));
			nodos.add(raizActual.getValue());
			System.out.println("por la izquierda");
			cadena = "por la izquierda de " + raizActual.getValue() + " --> ";
			imprimirizq(raizActual.getLeft());
			System.out.println("por la derecha");
			cadena = "por la derecha de "+ raizActual.getValue() + " --> ";
			imprimirder(raizActual.getRight());
			imprimirArbol(raizActual.getLeft());
			imprimirArbol(raizActual.getRight());
		}
	}

	private void imprimirder(NodoArbol<K, V> derActual) 
	{
		if(derActual != null)
		{
			System.out.println(derActual.getValue());
			cadena = cadena + derActual.getValue();
			objetos.add((V) cadena);
			nodos.add(derActual.getValue());
			imprimirder(derActual.getRight());
		}
		else
		{
			System.out.println("Nada");
		}
	}

	private void imprimirizq(NodoArbol<K, V> izqActual) 
	{
		if(izqActual != null)
		{
			System.out.println(izqActual.getValue());
			cadena = cadena + izqActual.getValue();
			objetos.add((V) cadena);
			nodos.add(izqActual.getValue());
			imprimirizq(izqActual.getLeft());
		}
		else
		{
			System.out.println("Nada");
		}
	}
	
	public ArrayList<V> getObjects()
	{
		return objetos;
	}
	
	public ArrayList<V> getNodes()
	{
		return nodos;
	}

}
